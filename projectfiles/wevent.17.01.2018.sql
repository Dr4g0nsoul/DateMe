-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 17. Jan 2018 um 23:31
-- Server-Version: 10.1.28-MariaDB
-- PHP-Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `wevent`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eventparticipants`
--

CREATE TABLE `eventparticipants` (
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `eventparticipants`
--

INSERT INTO `eventparticipants` (`user_id`, `event_id`) VALUES
(9, 1),
(9, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `user_host` int(11) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `thumbnail` varchar(500) DEFAULT NULL,
  `participant_count` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `public` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `events`
--

INSERT INTO `events` (`id`, `user_host`, `title`, `description`, `thumbnail`, `participant_count`, `start_date`, `end_date`, `location_id`, `deleted`, `public`) VALUES
(1, 4, 'Furst Event', 'We take YOU to a Magical Journey with our special Mushrooms.', 'colony_7_by_arthurblue-db3r00s.jpgtest2018-01-13.jpg', 1, '2018-01-13 12:12:00', '2018-01-14 13:13:00', NULL, 0, 1),
(2, 4, 'Destroy Net Neutrality with Ajit Pal', 'Fake News', 'ajit_pai.jpgtest2018-01-14.jpg', 1, '2018-01-14 12:12:00', '2018-01-15 13:13:00', NULL, 0, 1),
(4, 4, 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 'bbhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', NULL, 0, '2018-01-14 09:09:00', NULL, NULL, 0, 1),
(5, 4, 'Going on vacation', 'Vamos alla playa', 'waterfall_wallpaper_01_1920x1080.jpgtest2018-01-21.jpg', 0, '2018-01-21 12:12:00', NULL, NULL, 0, 0),
(6, 9, 'My Private event', 'My Private description', '23880853712_de2c14ed08_o.jpgtom2018-01-13.jpg', -7, '2018-01-13 12:12:00', NULL, NULL, 1, 0),
(7, 9, '2018-01-14 21:40:31', '2018-01-14 21:40:31', NULL, -2, '2018-01-21 23:04:00', NULL, NULL, 1, 0),
(8, 9, 'Event with location', 'We are here', '600528.pngtom2018-01-17.png', 0, '2018-01-17 09:00:00', NULL, 1, 1, 1),
(9, 9, 'Location Event', 'Location', 'link_painting.jpgtom2018-01-17.jpg', 0, '2018-01-17 09:00:00', NULL, 2, 1, 0),
(10, 9, 'Test Loc', 'Jetz miasetz gian', '600528.pngtom2018-01-17.png', 0, '2018-01-17 09:00:00', NULL, NULL, 1, 0),
(11, 9, 'asd', 'asd', '600528.pngtom2018-01-17.png', 0, '2018-01-17 09:09:00', NULL, 3, 1, 0),
(12, 9, 'Far away', 'Far away', NULL, 0, '2018-01-19 11:11:00', NULL, 4, 0, 1),
(13, 9, 'Near to me', 'Near', NULL, 0, '2018-01-21 11:11:00', NULL, 5, 0, 1),
(14, 9, '2018-01-15 17:44:33', '2018-01-15 17:44:33', NULL, 0, '2018-01-15 12:12:00', NULL, NULL, 1, 0),
(15, 9, 'asddddddd', 'asddddddddddd', NULL, 0, '2018-01-16 12:12:00', NULL, NULL, 1, 0),
(16, 9, 'test', 'test', NULL, 0, '2018-01-16 13:13:00', NULL, NULL, 1, 0),
(17, 9, 'ccccc', 'ccccccccc', NULL, 0, '2018-01-16 15:15:00', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `event_review`
--

CREATE TABLE `event_review` (
  `id` int(11) NOT NULL,
  `stars` int(11) DEFAULT '-1',
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0',
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `locations`
--

INSERT INTO `locations` (`id`, `description`, `longitude`, `latitude`) VALUES
(1, '39044 Neumarkt, Province of Bolzano - South Tyrol, Italy', 46.3115896, 11.272514),
(2, '39044 Neumarkt, Province of Bolzano - South Tyrol, Italy', 46.3115896, 11.272514),
(3, '39044 Neumarkt, Province of Bolzano - South Tyrol, Italy', 11.272514, 46.3115896),
(4, 'New York, NY, USA', -74.0059728, 40.7127753),
(5, '39100 Bolzano, Province of Bolzano - South Tyrol, Italy', 11.3547582, 46.4982953);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `user_from` int(11) DEFAULT NULL,
  `user_to` int(11) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `msg_read` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_to` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `notification_text` varchar(500) DEFAULT NULL,
  `important` tinyint(1) DEFAULT '0',
  `notification_read` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `avatar` varchar(300) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `public_stars` int(11) DEFAULT '0',
  `private_stars` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`, `location_id`, `public_stars`, `private_stars`, `status`, `deleted`) VALUES
(4, 'test', 'afc2c05fdf0e436786c3cf78d10bbfe4ee16ec8f8ac3a011b1a2279a0afa164c', 'c@c.com', 'test.png', NULL, 0, 0, 0, 0),
(9, 'Tom', 'f8c83ae5aa1f9957b752a5f7ce161e8573c1f41acd74da37b9aac32280912aac', 'Kappa@123.com', 'Tom.jpg', NULL, 0, 0, 0, 0);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `eventparticipants`
--
ALTER TABLE `eventparticipants`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indizes für die Tabelle `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_host` (`user_host`),
  ADD KEY `events_ibfk_2` (`location_id`);

--
-- Indizes für die Tabelle `event_review`
--
ALTER TABLE `event_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indizes für die Tabelle `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_from` (`user_from`),
  ADD KEY `user_to` (`user_to`);

--
-- Indizes für die Tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_to` (`user_to`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `location_id` (`location_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT für Tabelle `event_review`
--
ALTER TABLE `event_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `eventparticipants`
--
ALTER TABLE `eventparticipants`
  ADD CONSTRAINT `eventparticipants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eventparticipants_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`user_host`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints der Tabelle `event_review`
--
ALTER TABLE `event_review`
  ADD CONSTRAINT `event_review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `event_review_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON UPDATE CASCADE;

--
-- Constraints der Tabelle `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints der Tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints der Tabelle `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
