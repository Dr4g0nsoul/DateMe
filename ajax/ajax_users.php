<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');

//login
if(isset($_POST) && !empty($_POST["login"]) && !empty($_POST["username"]) && !empty($_POST["password"])) {
    if($uname = user_login($_POST["username"],$_POST["password"])){
        $_SESSION["username"] = $uname;
        echo $_SESSION["username"];
    } else {
        echo "false";
    }
}

//register
if(isset($_POST) && !empty($_POST["register"]) && !empty($_POST["username"]) && !empty($_POST["password"]) && !empty($_POST["email"])) {
    if($uname = user_register($_POST["username"], $_POST["password"], $_POST["email"], "")){
        $_SESSION["username"] = $uname;
        echo $_SESSION["username"];
    } else {
        echo "false";
    }
}

//getuserinfo
if(isset($_POST) && !empty($_POST["userinfo"]) && !empty($_POST["username"])){
    if($usrinfo = getUserInfo($username)){
        echo json_encode($usrinfo);
    }
}

//change_email
if(isset($_POST) && !empty($_POST["changemail"]) && !empty($_POST["username"]) && !empty($_POST["email"])){
    table_update("users", array("email"), array($_POST["email"]), "username = '" . $_POST["username"] . "'");
}

//change_password
if(isset($_POST) && !empty($_POST["changepassword"]) && !empty($_POST["username"]) && !empty($_POST["oldpw"]) && !empty($_POST["newpw"])){
    if(user_change_password($_POST["username"], $_POST["oldpw"], $_POST["newpw"])){
        echo "true";
    } else {
        echo "false";
    }
}
?>
