<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/events.php');
if(!empty($_POST)&&!empty($_POST["showevent"])&&!empty($_POST["showevent_id"])){
    require_once($_SERVER['DOCUMENT_ROOT'].'/view/eventview.php');
}

if(!empty($_POST)&&!empty($_POST["joinevent"])&&!empty($_POST["user_id"])&&!empty($_POST["event_id"])){
    echo joinEvent(intval($_POST["user_id"]), intval($_POST["event_id"]));
}

if(!empty($_POST)&&!empty($_POST["leaveevent"])&&!empty($_POST["user_id"])&&!empty($_POST["event_id"])){
    echo leaveEvent(intval($_POST["user_id"]), intval($_POST["event_id"]));
}

if(!empty($_POST)&&!empty($_POST["deleteevent"])&&!empty($_POST["eventid"])){
    echo deleteEvent(intval($_POST["eventid"]));
}

if(!empty($_GET["eventlisting"]) && !empty($_GET["listingmethod"])){
    require_once($_SERVER["DOCUMENT_ROOT"].'/scripts/events.php');
    $search = "";
    if(!empty($_GET["search"])&&strlen($_GET["search"]) > 2){
        $search = $_GET["search"];
    }
    $evt_results = array();
    if(strtolower($_GET["listingmethod"]) === "all"){
        $evt_results = getPublicEvents($search);
    }
    if(strtolower($_GET["listingmethod"]) === "popular"){
        $evt_results = getPopularEvents(false, $search);
    }
    if(strtolower($_GET["listingmethod"]) === "newest"){
        $evt_results = getNewestEvents($search);
    }
    if(strtolower($_GET["listingmethod"]) === "nearest" && isset($_GET["lat"]) && isset($_GET["long"])){
        $evt_results = getNearestEvents($_GET["lat"], $_GET["long"], $search);
    }
    foreach($evt_results as $curr){
?>
<div class="card">
<div class="card-header">
    <span class="float-left text-info text-right mdi mdi-calendar" style="padding-top: 4px;"> <?= date("d-m-Y", strtotime($curr["start_date"]));?>  <span class="mdi mdi-clock"> <?= date("H:i", strtotime($curr["start_date"]));?></span></span><span class="float-right"><button class="btn btn-sm btn-info showevent" eventid="<?= $curr["id"];?>">More info...</button></span>
</div>
    <div class="card-body">
        <h5 class="card-title"><?= $curr["title"];?></h5>
        <div class="row">
<?php
        if(!empty($curr["thumbnail"])){
?>
            <div class="col-lg-3">
            <img class="mr-3 img-fluid" src="img/event_thumb/<?= $curr["thumbnail"];?>" width="200px" height="200px" alt="Generic placeholder image">
            </div>
            <div class="col-lg-9">
                <div class="row">
                <p class="mr-3">
                     <?= $curr["description"];?>
                </p>
                </div>
            </div>
<?php
    } else {
?>
            <div class="col-lg-12">
                <div class="row">
                <p class="mr-3">
                     <?= $curr["description"];?>
                </p>
                </div>
            </div>
<?php
    }
?>
        </div>
    </div>

</div>
<br>
<?php
    }
}


if(!empty($_POST)&&!empty($_POST["evtcal"])&&!empty($_POST["month"])&&!empty($_POST["year"])){
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');
    $user_id = getUserInfo($_SESSION["username"]);
    $user_id = intval($user_id["id"]);
    echo json_encode(calendarfetch($user_id, intval($_POST["month"]), intval($_POST["year"])));
}

if(!empty($_POST)&&!empty($_POST["evtsched"])&&!empty($_POST["month"])&&!empty($_POST["year"])){
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');
    $user_id = getUserInfo($_SESSION["username"]);
    $user_id = intval($user_id["id"]);
    echo json_encode(schedulefetch($user_id, intval($_POST["month"]), intval($_POST["year"])));
}
?>
