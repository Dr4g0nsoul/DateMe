/*jslint browser: true*/ /*global  $ document useraway:true profilelocation:true*/

useraway = 0;
profilelocation = "";

function myContentLoad(nextMenu) {
    $.ajax({
        url : 'ajax/ajax_menu.php',
        type : 'GET',
        data : {
            menu : nextMenu
        },
        dataType : 'html',
        async : true,
        success : function(result) {
            $("#mycontent").empty();
            $("#mycontent").append(result);
            if(nextMenu == "profile"){
                if(profilelocation.length > 0) {
                    $("#profile-loc").text(profilelocation);
                } else {
                    $("#profile-loc").text("None yet");
                }
            }
        }
    });
}

function showEventDetailDialog(evtid) {
    $.ajax({
        url : 'ajax/ajax_event.php',
        type : 'POST',
        data : {
            showevent : "showevent",
            showevent_id : evtid
        },
        dataType : 'html',
        async : true,
        success : function(result) {
            $("#eventviewrapper").empty();
            $("#eventviewrapper").append(result);
            $("#tse").modal('show');
        }
    });
}

$(document).ready(function () {
    $(document).on("click", "#nav-home", function () {
        myContentLoad("home");
    });
    $(document).on("click", "#nav-profile", function () {
        myContentLoad("profile");
    });
    $(document).on("click", "#nav-events", function () {
        myContentLoad("events");
    });
    $(document).on("click", "#nav-schedule", function () {
        myContentLoad("schedule");
    });
    $(document).on("click", "#create-new-event", function(){
        myContentLoad("create-event");
    });

    $(document).on("click", ".showevent", function(){
        showEventDetailDialog($(this).attr("eventid"));
    });

    if($("#loadprofilenow").length){
        $("#loadprofilenow").remove();
        myContentLoad("profile");
    }
});
