/*jslint browser: true*/ /*global  $ document google*/
function showThumbnail(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb').attr('src', e.target.result);
            $('#thumb').slideDown("slow");
        };

        reader.readAsDataURL(input.files[0]);
    }
}

    function checkdateisright(){
        var ret = false;
        if($("#ec-no-end").is(":checked")){
            ret = true;
        } else {
            var st_date = new Date($("#ec-start-date").val());
            var end_date = new Date($("#ec-end-date").val());
            if(st_date.getTime() < end_date.getTime()) {
                ret = true;
            } else
                if(st_date.getTime() === end_date.getTime()) {
                    if(getTimeInputInSec($("#ec-start-time").val()) > getTimeInputInSec($("#ec-end-time").val())){
                        ret = false;
                    } else {
                        ret = true;
                    }
                } else {
                    ret = false;
                }
        }
        if(ret === false){
            $("#global-danger-alert").text("Invalid end date!");
            if(!$("#global-danger-alert").is(":visible")){
                $("#global-danger-alert").slideDown("slow");
            }
        } else {
            $("#global-danger-alert").hide();
            $("#global-danger-alert").empty();
        }
        return ret;
    }

    function getTimeInputInSec(timeInput){
        var a = timeInput.split(':');
        return (+a[0]) * 60 * 60 + (+a[1]) * 60;
    }

$(document).ready(function(){
    $(document).on("click","#ec-reset",function(){$("#thumb").fadeOut("slow");});
    $(document).on("click", "#ec-validate-loc", function (){
        if($.trim($("#ec-location").val()).length > 0) {
            $.ajax({
                url : 'https://maps.googleapis.com/maps/api/geocode/json',
                type : 'GET',
                data : {
                    address : $.trim($("#ec-location").val()),
                    key : "AIzaSyAq_lAkzR0cxFwFBxdHd642EcZ5frwR2IA"
                },
                dataType : 'json',
                async : true,
                success : function(result) {
                    $("#ec-location").val(JSON.stringify(result["results"][0]["formatted_address"]).replace(/\"/g, ""));
                    $("#ec-loc-fg").empty();
                    $("#ec-loc-fg").append("<input type='text' id='ec-lat' name='ec-lat' value='" + result["results"][0]["geometry"]["location"]["lat"]  + "' readonly>");
                    $("#ec-loc-fg").append("<input type='text' id='ec-long' name='ec-long' value='" + result["results"][0]["geometry"]["location"]["lng"]  + "' readonly>");
                    if($("#ec-map").is(":visible")){
                        var longlat = {lat: parseFloat($("#ec-lat").val()), lng: parseFloat($("#ec-long").val())};
                        var map = new google.maps.Map(document.getElementById('ec-map'), {
                          zoom: 12,
                          center: longlat
                        });
                        var marker = new google.maps.Marker({
                          position: longlat,
                          map: map
                        });
                    } else {
                        $("#ec-map").slideDown("slow", function (){
                            var longlat = {lat: parseFloat($("#ec-lat").val()), lng: parseFloat($("#ec-long").val())};
                            var map = new google.maps.Map(document.getElementById('ec-map'), {
                              zoom: 12,
                              center: longlat
                            });
                            var marker = new google.maps.Marker({
                              position: longlat,
                              map: map
                            });
                        });
                    }
                }
            });
        }
    });
    $(document).on("change", "#ec-no-end", function (){
       if($("#ec-no-end").is(":checked")){
           $("#fc-end-date").slideUp("slow");
           $("#ec-end-date").removeAttr("required");
           $("#ec-end-date").prop("disabled","disabled");
           $("#ec-end-time").removeAttr("required");
           $("#ec-end-time").prop("disabled","disabled");
       } else {
           $("#fc-end-date").slideDown("slow");
           $("#ec-end-date").prop("required", "required");
           $("#ec-end-date").removeAttr("disabled");
           $("#ec-end-time").prop("required", "required");
           $("#ec-end-time").removeAttr("disabled");
       }
    });
});
