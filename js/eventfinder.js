/*jslint browser: true*/ /*global  $ document navigator*/
$(document).ready(function (){
    $(document).on("change", "input[type=radio][name='evt-order']", function(){
        if($("#evt-order-all").is(":checked")){
            $.ajax({
                url : 'ajax/ajax_event.php',
                type : 'GET',
                data : {
                    eventlisting : "eventlisting",
                    listingmethod : "all",
                    search : $("#evt-search-text").val()
                },
                dataType : 'html',
                async : true,
                success : function(result) {
                    $("#evt-results-wrapper").empty();
                    $("#evt-results-wrapper").append(result);
                }
            });
        }
        if($("#evt-order-popular").is(":checked")){
            $.ajax({
                url : 'ajax/ajax_event.php',
                type : 'GET',
                data : {
                    eventlisting : "eventlisting",
                    listingmethod : "popular",
                    search : $("#evt-search-text").val()
                },
                dataType : 'html',
                async : true,
                success : function(result) {
                    $("#evt-results-wrapper").empty();
                    $("#evt-results-wrapper").append(result);
                }
            });
        }
        if($("#evt-order-newest").is(":checked")){
            $.ajax({
                url : 'ajax/ajax_event.php',
                type : 'GET',
                data : {
                    eventlisting : "eventlisting",
                    listingmethod : "newest",
                    search : $("#evt-search-text").val()
                },
                dataType : 'html',
                async : true,
                success : function(result) {
                    $("#evt-results-wrapper").empty();
                    $("#evt-results-wrapper").append(result);
                }
            });
        }
        if($("#evt-order-nearby").is(":checked")){
            if (navigator.geolocation){
                navigator.geolocation.getCurrentPosition(function(position){
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;
                    $.ajax({
                        url : 'ajax/ajax_event.php',
                        type : 'GET',
                        data : {
                            eventlisting : "eventlisting",
                            listingmethod : "nearest",
                            lat : lat,
                            long : long,
                            search : $("#evt-search-text").val()
                        },
                        dataType : 'html',
                        async : true,
                        success : function(result) {
                            $("#evt-results-wrapper").empty();
                            $("#evt-results-wrapper").append(result);
                        }
                    });
                });
            }
        }
    });
    $(document).on("keypress",'#evt-search-text',function(e){
        if(e.which == 13){
            $('#evt-search').click();
        }
    });
    $(document).on("keydown",'#evt-search-text',function(e){
        if(e.which == 27){
            $('#evt-search-text').val("");
            $('#evt-search').click();
        }
    });
    $(document).on("click", "#evt-search", function(){
        $("input[type=radio][name='evt-order']").change();
    });
});
