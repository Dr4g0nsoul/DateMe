/*jslint browser: true*/ /*global  $ document currentSchedMonth:true currentSchedYear:true currentSchedDay:true*/

currentSchedMonth = (new Date($.now())).getMonth();
currentSchedYear = (new Date($.now())).getFullYear();
currentSchedDay = 1;

function addLeadingZero(number){
    if(number < 10){
        return "0" + number;
    }
    return "" + number;
}

function createScheduleTableEntry(eid, title, start_date, public, host){
    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var appendString = "";
    var dateTime = start_date.split(" ");
    var dateOnly = dateTime[0];
    var timeOnly = dateTime[1];

    var temp = dateOnly + "T" + timeOnly;
    var sdate = new Date(temp);
    if(sdate.getDate() >= currentSchedDay){
        currentSchedDay = sdate.getDate() + 1;
        var dayString = dayNames[sdate.getDay()] + " " + addLeadingZero(sdate.getDate()) + "." + addLeadingZero(sdate.getMonth() + 1) + "." + sdate.getFullYear();
        appendString = appendString + "<tr class='table-primary'><td class='mdi mdi-calendar'> " + dayString + "</td><td></td><td></td></tr>";
    }
    var pubprivstring = "sched-priv";
    if(public == 1){
        pubprivstring = "sched-pub";
    }

    appendString = appendString + "<tr class='" + pubprivstring + "'><td class='mdi mdi-clock'> " + addLeadingZero(sdate.getHours()) + ":" + addLeadingZero(sdate.getMinutes()) + "</td>";
    appendString = appendString + "<td>" + title + "</td>";
    appendString = appendString + "<td class='text-right'><button eventid='" + eid + "' class='btn btn-sm btn-light mdi mdi-magnify showevent'></button> <button hostid='" + host + "' eventid='" + eid + "' class='btn btn-sm btn-light mdi mdi-close-circle-outline deleteevent'></button></td></tr>";
    $("#sched-table").append(appendString);
}

function loadScheduleIntoTable(){
    currentSchedDay = 1;
    $("#sched-table").empty();
    var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
                    ];
    $("#sched-month").val(monthNames[currentSchedMonth] + " - " + currentSchedYear);
    $.ajax({
        url : 'ajax/ajax_event.php',
        type : 'POST',
        data : {
            evtsched : "evtsched",
            month : currentSchedMonth+1,
            year : currentSchedYear
        },
        dataType : 'json',
        async : true,
        success : function(result) {
            for (var i = 0; i < result.length; i++) {
                createScheduleTableEntry(result[i]["id"], result[i]["title"], result[i]["start_date"], result[i]["public"], result[i]["host"]);
            }
        }
    });
}

$(document).ready(function (){
    $(document).on("click", ".deleteevent", function (){
        var confQuestion = "";
        var del = false;
        if($("#schedusrid").val() == $(this).attr("hostid")){
            confQuestion = "Do you want to delete your event? (Every participant will be kicked from the Event)";
            del = true;
        } else {
            confQuestion = "Do you want to leave the event?";
        }
        var r = confirm(confQuestion);
        if(r == true){
            if(del == false){
                //leave event
                $.ajax({
                    url : 'ajax/ajax_event.php',
                    type : 'POST',
                    data : {
                        leaveevent : "leaveevent",
                        user_id : $("#schedusrid").val(),
                        event_id : $(this).attr("eventid")
                    },
                    dataType : 'text',
                    async : true,
                    success : function(result) {
                        setupCalendar();
                        loadScheduleIntoTable();
                    }
                });
            } else {
                //Remove event
                $.ajax({
                    url : 'ajax/ajax_event.php',
                    type : 'POST',
                    data : {
                        deleteevent : "deleteevent",
                        eventid : $(this).attr("eventid")
                    },
                    dataType : 'text',
                    async : true,
                    success : function(result) {
                        setupCalendar();
                        loadScheduleIntoTable();
                    }
                });
            }
        }
    });

    $(document).on("click", "#sched-previous", function (){
        currentSchedMonth -= 1;
        if(currentSchedMonth == -1){
            currentSchedYear -= 1;
            currentSchedMonth = 11;
        }
        currentSchedDay = 1;
        $('#sched-table').empty();
        loadScheduleIntoTable();
    });

    $(document).on("click", "#sched-next", function (){
        currentSchedMonth += 1;
        if(currentSchedMonth == 12){
            currentSchedYear += 1;
            currentSchedMonth = 0;
        }
        currentSchedDay = 1;
        $('#sched-table').empty();
        loadScheduleIntoTable();
    });
    currentSchedDay = 1;
    $('#sched-table').empty();
    loadScheduleIntoTable();
});
