/*jslint browser: true*/ /*global  $ document navigator profilelocation:true*/


function showNewProfilePic(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profpic').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function resetmypic() {
    $("#profilepicform")[0].reset();
    $('#profpic').attr('src', $("#lastpic").val());
}

$(document).ready(function (){
    if(useraway == 0){
        $("#profile-away-state").text("Online");
    } else {
        $("#profile-away-state").text("Away");
    }


    $(document).on("click", "#changemail", function(){
        var mailbtn = $("#changemail");
        var mailtd = $("#mailtd");
        if(mailbtn.hasClass("now-save-btn")){
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test($("#newmailinput").val())){
                //saving
                var lastmailinput = $("#newmailinput").val();
                mailbtn.removeClass("now-save-btn");
                mailbtn.removeClass("mdi-content-save");
                mailbtn.addClass("mdi-pencil");

                $.ajax({
                    url : 'ajax/ajax_users.php',
                    type : 'POST',
                    data : {
                        changemail : "changemail",
                        username : $("#usr_name").text(),
                        email : $("#newmailinput").val()
                    },
                    dataType : 'text',
                    async : true,
                    success : function() {
                        mailtd.empty();
                        mailtd.text(lastmailinput);
                    }
                });
            }
        } else {
            var lastmail = mailtd.text();
            //change to edit mode
            mailbtn.addClass("now-save-btn");
            mailbtn.removeClass("mdi-pencil");
            mailbtn.addClass("mdi-content-save");
            mailtd.empty();
            mailtd.append($.parseHTML("<input class='form-control' id='newmailinput' value='" + lastmail + "'>"));
        }
    });

    $(document).on("click", "#chngpw", function (){
        if($.trim($("#newpw").val()) === $.trim($("#rpnewpw").val()) && $.trim($("#newpw").val()).length >= 0) {
            var pw = $.trim($("#newpw").val());
            if(pw.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)){
                $.ajax({
                    url : 'ajax/ajax_users.php',
                    type : 'POST',
                    data : {
                        changepassword : "changepassword",
                        username : $.trim($("#usr_name").text()),
                        oldpw : $.trim($("#oldpw").val()),
                        newpw : $.trim($("#newpw").val())
                    },
                    dataType : 'text',
                    async : true,
                    success : function(result) {
                        if(result.toLowerCase() === "false") {
                            $("#pw_danger_alert").text("The original password is wrong.");
                            if(!$("#pw_danger_alert").is(":visible")) {
                                $("#pw_danger_alert").slideDown("slow");
                            }
                        } else {
                            $('#mod-passwd-reset').modal('hide');
                            $("#pw_success_alert").slideDown("slow");
                            setTimeout(function(){
                               $("#pw_success_alert").slideUp("fast");
                            }, 5000);
                        }
                    }
                });
            } else {
                $("#pw_danger_alert").text("The password has to atleast contain one Uppercase, one lowercase character, one number and it has to  be atleas 8 characters long.");
                if(!$("#pw_danger_alert").is(":visible")) {
                    $("#pw_danger_alert").slideDown("slow");
                }
            }
        } else {
            $("#pw_danger_alert").text("The two passwords are not coinciding.");
            if(!$("#pw_danger_alert").is(":visible")) {
                $("#pw_danger_alert").slideDown("slow");
            }
        }
    });

    $(document).on("click", "#profile-away", function (){
        if(useraway == 0){
            useraway = 1;
            $("#profile-away-state").text("Away");
        } else {
            useraway = 0;
            $("#profile-away-state").text("Online");
        }
    });

    $(document).on("click", "#profile-loc-search", function (){
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
                var latlng = position.coords.latitude+", "+ position.coords.longitude;
                var lat = position.coords.latitude;
                var long = position.coords.longitude;
                $.ajax({
                    url : 'https://maps.googleapis.com/maps/api/geocode/json',
                    type : 'GET',
                    data : {
                        latlng : latlng,
                        key : "AIzaSyAq_lAkzR0cxFwFBxdHd642EcZ5frwR2IA"
                    },
                    dataType : 'json',
                    async : true,
                    success : function(result) {
                        profilelocation = JSON.stringify(result["results"][0]["formatted_address"]).replace(/\"/g, "");
                        $("#profile-loc").text(profilelocation);
                    }
                });
            });
        } else {
            $('#profile-loc').text("GPS not detected");
            $('#profile-loc-search').prop("disabled",true);
        }
    });



});
