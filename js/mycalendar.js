/*jslint browser: true*/ /*global  $ document currentMonth:true currentYear:true*/
currentMonth = (new Date($.now())).getMonth();
currentYear = (new Date($.now())).getFullYear();

function paintEventIntoCalendar(id, day, public){
    $("#mycal td").each(function(){
        var td_day = $(this).attr('day');

        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if (typeof td_day !== typeof undefined && td_day !== false && td_day == day) {
            if(public == 1){
                $(this).addClass("calpub");
            } else {
                $(this).addClass("calpriv");
            }
        }
    });
}

function loadEventsIntoCalendar(){
    $.ajax({
        url : 'ajax/ajax_event.php',
        type : 'POST',
        data : {
            evtcal : "evtcal",
            month : currentMonth+1,
            year : currentYear
        },
        dataType : 'json',
        async : true,
        success : function(result) {
            for (var i = 0; i < result.length; i++) {
                paintEventIntoCalendar(result[i]["id"], result[i]["day"], result[i]["public"]);
            }
        }
    });
}

function setupCalendar() {
    $('#mycal').empty();
    var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
                    ];
    var today = new Date($.now());
    var cal = $('#mycal');
    //get information
    var month = monthNames[currentMonth];
    var lastDayOfMonth = (new Date(currentYear, currentMonth+1, 0)).getDate();
    var lastDayOfPrevMonth = (new Date(currentYear, currentMonth, 0)).getDate();
    //create calendar
    var includeString = "<tbody><tr> \
                <th colspan='2'><button id='prevMonth'><span class='mdi mdi-arrow-left-bold'></span></button></th> \
                <th colspan='3'> " + month +" - " + currentYear +"</th> \
                <th colspan='2'><button id='nextMonth'><span class='mdi mdi-arrow-right-bold'></span></button></th> \
            </tr> \
            <tr> \
                <th>Mon</th> \
                <th>Tue</th> \
                <th>Wed</th> \
                <th>Thi</th> \
                <th>Fri</th> \
                <th>Sat</th> \
                <th>Sun</th> \
            </tr><tr>";
    var firsttime = true;
    var firstIndex = 0;
    for(var i = 0; i<35; i++){
        if(firsttime){
            firsttime = false;
            var firstDay = new Date(currentYear,currentMonth,1);
            if(firstDay.getDay() == 0){
                firstIndex = 6;
            } else {
                firstIndex = firstDay.getDay() - 1;
            }
        }
        if(i+1 === today.getDate() && currentMonth === today.getMonth() && currentYear === today.getFullYear()){
            includeString += "<td day='"+ (i+1) +"' class='today'>"+ (i+1) +"</td>";
        } else if(i < firstIndex) {
            includeString += "<td class='prevMonth'>"+ (lastDayOfPrevMonth - (firstIndex - i) + 1) +"</td>";
        } else if(i+1 > lastDayOfMonth) {
            includeString += "<td class='nextMonth'>"+ ((i+1)-lastDayOfMonth) +"</td>";
        } else {
            includeString += "<td day='"+ (i+1 - firstIndex) +"'>"+ (i+1 - firstIndex) +"</td>";
        }
        if((i+1) % 7 === 0 && i < 34){
            includeString += "</tr><tr>";
        }
    }
    includeString += "</tr></tbody>";
    cal.append(includeString);
    loadEventsIntoCalendar();
}

$(document).ready(function (){

    setupCalendar();

    //aply next and previuos month button events
    $(document).on('click','#prevMonth',function() {
        currentMonth -= 1;
        if(currentMonth == -1){
            currentYear -= 1;
            currentMonth = 11;
        }
        $('#mycal').empty();
        setupCalendar();
    });
    $(document).on('click','#nextMonth',function() {
        currentMonth += 1;
        if(currentMonth == 12){
            currentYear += 1;
            currentMonth = 0;
        }
        $('#mycal').empty();
        setupCalendar();
    });

});
