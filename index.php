<?php
header('Access-Control-Allow-Origin: *');
session_start();
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/config.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');

//profile pic upload
$uploadedpic = false;
if(!empty($_FILES['profilepicupload']["name"])) {

    // Create directory if it does not exist
    if(!is_dir("img/profilepics/")) {
        mkdir("img/profilepics/", 0777, true);
    }
    $avatar = basename(strtolower(strtr($_FILES['profilepicupload']["name"], array(' ' => '_','ä' => 'ae','ö' => 'oe','ü' => 'ue','ß' => 'ss'))));
    $avatar = $_SESSION["username"] . substr($avatar,strrpos($avatar, '.'));
    move_uploaded_file($_FILES['profilepicupload']['tmp_name'], "img/profilepics/".$avatar);
    table_update("users", array("avatar"), array($avatar), "username = '" . $_SESSION["username"] . "'");
    $uploadedpic = true;
}
$profilepic = "";
if(!empty($_SESSION["username"])){

    $profilepic = getUserInfo($_SESSION["username"]);
    if(!empty($profilepic["avatar"])){
        $profilepic = $profilepic["avatar"];
    } else {
        $profilepic = "";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Custom CSS goes here -->
    <link rel="stylesheet" href="css/materialdesignicons.min.css">
    <link rel="stylesheet" href="css/CalAndSchedStryle.css">
    <!-- Bootsrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Custom JS goes here -->
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= $google_key?>"></script>
    <script src="js/menuloader.js"></script>
    <script src="js/mycalendar.js"></script>
    <script src="js/schedule.js"></script>
    <script src="js/profilepage.js"></script>
    <script src="js/eventfinder.js"></script>
    <script src="js/createevent.js"></script>
    <title>WeVent</title>
</head>
<body>
<?php
    if(!empty($_SESSION) && !empty($_SESSION["username"])) {
        if($uploadedpic){
            echo "<input type='hidden' id='loadprofilenow'>";
        }
?>
  <!-- Icon menu -->

   <!-- Navigation bar -->
    <nav class="navbar navbar-expand-lg navbar-light ml-auto" style="background-color: #efefef;">
        <a class="navbar-brand text-primary" href="#" style="margin-left: 5%;">Wevent</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menulist" aria-controls="menulist" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse show" id="menulist" style="">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item ">
                <a class="nav-link" id="nav-home" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" id="nav-profile" href="#">Profile</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" id="nav-events" href="#">Events</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" id="nav-schedule" href="#">Schedule</a>
                </li>
            </ul>

        </div>


            <div class="ml-auto dropdown"  style="margin-right: 5%;" id="navusermenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <span class="text-primary"> <?= $_SESSION["username"];?> &emsp;</span><?php
if(empty($profilepic)){
    echo "<img src='img/default-pic.png' alt='<Profile Pic>' class='mr-3 img-fluid border border-primary rounded-circle' id='profile-profile-pic' style='margin-bottom: 5px; max-width:50px; max-height: 50px;'>";
} else {
    echo "<img src='img/profilepics/". $profilepic ."' alt='<Profile Pic>' class='mr-3 img-fluid border border-primary rounded-circle' id='profile-profile-pic' style='margin-bottom: 5px; max-width:50px; max-height: 50px;'>";
}
                ?>
               <div class="dropdown-menu" aria-labelledby="navusermenu">
                <button class="dropdown-item mdi mdi-logout-variant" onclick="location.href='logout.php';"> Logout</button>
              </div>
            </div>
    </nav>
 <!-- End of navigation bar-->

  <!-- Contents -->
<div class="alert alert-success" id="global-success-alert" style="display: none;"></div>
<div class="alert alert-danger" id="global-danger-alert" style="display: none;"></div>
    <br>
    <div class="row">
       <div class="col-sm-1"></div>
        <div class="col-sm-8 mx-auto" id="mycontent">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/view/home.php');?>
        </div>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/view/right_sidebar.php');?>
    </div>
    <script>
    showScriptNotLoaded = true;
    </script>
    <div id="eventviewrapper">

    </div>
   <footer>
            <?php require($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>
    </footer><?php
    } else {
        require_once($_SERVER['DOCUMENT_ROOT'].'/view/loginregister.php');
    }
    ?>
</body>
</html>
