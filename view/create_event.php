<script>
if($("#ec-no-end").is(":checked")){
           $("#fc-end-date").slideUp("slow");
           $("#ec-end-date").removeAttr("required");
           $("#ec-end-date").prop("disabled","disabled");
           $("#ec-end-time").removeAttr("required");
           $("#ec-end-time").prop("disabled","disabled");
       } else {
           $("#fc-end-date").slideDown("slow");
           $("#ec-end-date").prop("required", "required");
           $("#ec-end-date").removeAttr("disabled");
           $("#ec-end-time").prop("required", "required");
           $("#ec-end-time").removeAttr("disabled");
       }
</script>
<form action="scripts/create-event-action.php" method="post" enctype="multipart/form-data" onsubmit="return checkdateisright();">
<div class="card">
    <div class="card-header bg-info text-white">Create event</div>
    <div class="card-body">
            <div class="form-group row">
                <label for="ec-username" class="col-xl-2 col-form-label">Host: </label>
                <div class="col-xl-10">
                  <input type="text" readonly class="form-control-plaintext" id="ec-username" name="ec-username" value="<?= $_SESSION["username"];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="ec-title" class="col-xl-2 col-form-label">Title: </label>
                <div class="col-xl-10">
                  <input type="text" class="form-control" id="ec-title" name="ec-title" placeholder="Title..." required>
                </div>
            </div>
            <div class="form-group row">
                <label for="ec-thumbnail" class="col-xl-2 col-form-label">Thumbnail: </label>
                <img id="thumb" src="" class="img img-thumbnail" alt="<Preview>" style="display: none; max-height: 100px; max-width:100%;">
                <div class="col-xl-10">
                   <input type="file" class="form-control-file" id="ec-thumbnail" onchange="showThumbnail(this);" name="ec-thumbnail" enctype="multipart/form-data">
                </div>
           </div>
           <div class="form-group row">
                <label for="ec-description" class="col-xl-2 col-form-label">Description: </label>
                <div class="col-xl-10">
                    <textarea type="text" class="form-control" id="ec-description" name="ec-description" placeholder="Description..." cols="3" rows="5" required></textarea>
                </div>
           </div>
           <div class="form-group row">
                <label for="ec-location" class="col-xl-2 col-form-label">Location: </label>
                <div class="col-xl-8">
                    <input type="text" class="form-control" id="ec-location" name="ec-location" placeholder="Input address..." cols="3" rows="5">
                    <small class="form-text text-muted">Location must be validated in order to be inserted.</small>
                </div>
                <div class="col-xl-2">
                    <button id="ec-validate-loc" type="button" class="btn btn-success mdi mdi-check" style="width: 100%;"> Check</button>
                </div>
           </div>
           <div id="ec-loc-fg" class="form-group row">

           </div>
           <div class="row">
               <div class="col-xl-2"></div>
               <div id="ec-map" class="col-xl-10" style="display:none; height: 500px;"></div>
           </div>
           <div class="form-group row">
                <label for="ec-start-date" class="col-xl-2 col-form-label">Start Date: </label>
                <div class="col-xl-5">
                    <input type="date" min="<?= date("Y-m-d");?>" class="form-control" id="ec-start-date" name="ec-start-date" required>
                </div>
                <div class="col-xl-5">
                    <input type="time" class="form-control" id="ec-start-time" name="ec-start-time" required>
                </div>
           </div>
           <div id="fc-end-date" class="form-group row">
                <label for="ec-end-date" class="col-xl-2 col-form-label">End Date: </label>
                <div class="col-xl-5">
                    <input type="date" min="<?= date("Y-m-d");?>" class="form-control" id="ec-end-date" name="ec-end-date" required>
                </div>
                <div class="col-xl-5">
                    <input type="time" class="form-control" id="ec-end-time" name="ec-end-time" required>
                </div>
           </div>
           <div class="form-group row">
               <label for="ec-no-end" class="col-xl-2 col-form-label">Same day: </label>
               <div class="col-xl-10">
                   <input type="checkbox" class="align-middle" id="ec-no-end" name="ec-no-end" checked>
               </div>
           </div>
           <div class="form-group row">
                <label for="" class="col-xl-2 col-form-label">Visibility: </label>
                <div class="col-xl-10">
                  <div class="form-group">
                      <div class="row" style="margin-left: 10px; margin-top: 8px;">

                       <div class="col-xl-6">
                          <div class="row">
                           <input class="form-check-input col-xl-2" type="radio" name="ec-visibility" id="ec-priv" value="private" checked>
                           <label class="form-check-label col-xl-10" for="ec-priv">Private</label>
                          </div>
                       </div>
                       <div class="col-xl-6">
                          <div class="row">
                           <input class="form-check-input col-xl-2" type="radio" name="ec-visibility" id="ec-pub" value="public">
                           <label class="form-check-label col-xl-10" for="ec-pub">Public</label>
                           </div>
                       </div>
                      </div>
                  </div>
                </div>
           </div>
    </div>
    <div class="card-footer">
        <div class="float-right">
            <button type="submit" class="btn btn-outline-primary">Create</button>&emsp;
            <button type="reset" class="btn btn-outline-secondary" id="ec-reset">Reset</button>
        </div>
    </div>
</div>
</form>
