<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/events.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');
    if(!empty($_POST)&&!empty($_POST["showevent"])&&!empty($_POST["showevent_id"])){
        if($currentEvent = getEvent(intval($_POST["showevent_id"]))){
            $curusrid = getUserInfo($_SESSION["username"]);
            $curusrid = intval($curusrid["id"]);
?>
 <div class="modal fade" id="tse" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<script>
if(showScriptNotLoaded){
    $(document).ready(function(){
       $(document).on("hidden.bs.modal", "#tse", function(){
           $("#addcal").unbind("click");
           $("#remcal").unbind("click");
           $("#tse").off("click");
           $("#tse").empty();
           $("#tse").remove();
       });
       $(document).on("click","#addcal",function(){
           $.ajax({
                url : 'ajax/ajax_event.php',
                type : 'POST',
                data : {
                    joinevent : "joinevent",
                    user_id : $("#addcal").attr("usr_id"),
                    event_id : $("#addcal").attr("evt_id")
                },
                dataType : 'text',
                async : true,
                success : function(result) {
                    $("#addcal").hide();
                    $("#remcal").fadeIn("slow");
                    $("#sepc").text(parseInt($("#sepc").text()) + 1);
                    setupCalendar();
                }
            });
       });
       $(document).on("click","#remcal",function(){
           $.ajax({
                url : 'ajax/ajax_event.php',
                type : 'POST',
                data : {
                    leaveevent : "leaveevent",
                    user_id : $("#remcal").attr("usr_id"),
                    event_id : $("#remcal").attr("evt_id")
                },
                dataType : 'text',
                async : true,
                success : function(result) {
                    $("#remcal").hide();
                    $("#addcal").fadeIn("slow");
                    $("#sepc").text(parseInt($("#sepc").text()) - 1);
                    setupCalendar();
                }
            });
       });
    });
    showScriptNotLoaded = false;
}
if($("#show-map").length){

$("#show-map").slideDown("slow", function (){
    var longlat = {lat: parseFloat($("#show-loc-info").attr("lat")), lng: parseFloat($("#show-loc-info").attr("long"))};
    console.log(parseFloat($("#show-loc-info").attr("lat")));
    console.log(parseFloat($("#show-loc-info").attr("long")));
    var map = new google.maps.Map(document.getElementById('show-map'), {
      zoom: 12,
      center: longlat
    });
    var marker = new google.maps.Marker({
      position: longlat,
      map: map
    });
});
}
</script>
  <div id="my-show-modal" class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= $currentEvent["title"];?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php if(!empty($currentEvent["thumbnail"])){?>
        <div class="row">
            <div class="col-lg-3">
                <img class="mr-3 img-fluid" src="img/event_thumb/<?= $currentEvent["thumbnail"];?>" width="200px" height="200px" alt="Generic placeholder image">
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                    <p><?= $currentEvent["description"];?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } else { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                    <p><strong>Description: </strong></p>
                    <p><?= $currentEvent["description"];?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <hr>
        <div class="row" style="margin-top: 10px; margin-bottom: 10px; <?php
        if($curusrid == $currentEvent["host"]){
            echo "display: none;";
        }
                                ?>">
            <div class="col-lg-3">
                <h6 >Join Event: </h6>
            </div>
            <div class="col-lg-9">
<?php
    if(isParticipating($curusrid, $currentEvent["id"])) {

?>
        <button id="addcal" usr_id="<?= $curusrid;?>" evt_id="<?= $currentEvent["id"];?>" class="btn btn-info btn-sm mdi mdi-plus" style="display: none;"> Add to calendar</button>
        <button id="remcal" usr_id="<?= $curusrid;?>" evt_id="<?= $currentEvent["id"];?>" class="btn btn-dark btn-sm mdi mdi-close-circle-outline"> Leave Event</button>
<?php
   } else {
?>
        <button id="addcal" usr_id="<?= $curusrid;?>" evt_id="<?= $currentEvent["id"];?>" class="btn btn-info btn-sm mdi mdi-plus"> Add to calendar</button>
        <button id="remcal" usr_id="<?= $curusrid;?>" evt_id="<?= $currentEvent["id"];?>" class="btn btn-dark btn-sm mdi mdi-close-circle-outline" style="display: none;"> Leave Event</button>
<?php
   }
?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 >Participants: </h6>
            </div>
            <div class="col-lg-9">
                <p id="sepc"><?= $currentEvent["participant_count"];?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 >Start date: </h6>
            </div>
            <div class="col-lg-9">
                <p><span class="mdi mdi-calendar"></span> <?= date("D d.m.Y",strtotime($currentEvent["start_date"]))?> &emsp;<span class="mdi mdi-clock"></span> <?= date("H:i",strtotime($currentEvent["start_date"]))?></p>
            </div>
        </div>
        <?php if(!empty($currentEvent["end_date"])){?>
        <div class="row">
            <div class="col-lg-3">
                <h6 >End date: </h6>
            </div>
            <div class="col-lg-9">
                <p><span class="mdi mdi-calendar"></span> <?= date("D d.m.Y",strtotime($currentEvent["end_date"]))?> &emsp;<span class="mdi mdi-clock"></span> <?= date("H:i",strtotime($currentEvent["end_date"]))?></p>
            </div>
        </div>
        <?php } if(!empty($currentEvent["location"])){?>
        <div class="row">
            <div class="col-lg-3">
                <h6 >Location: </h6>
            </div>
            <div class="col-lg-9">
                <p id="show-loc"><?= $currentEvent["location"]["desc"]?></p>
            </div>
        </div>
          <div class="row">
                <div id="show-loc-info" lat="<?= $currentEvent["location"]["lat"]?>" long="<?= $currentEvent["location"]["long"]?>" class="col-lg-3">

                </div>
                <div id="show-map" style="display: none; height: 500px;" class="col-lg-9">
                </div>
            </div>
            <?php }?>
      </div>
    </div>
  </div>
</div>
<?php
        }
    }
?>
