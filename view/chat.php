
      <div class="d-none d-md-block col-sm-2 ml-auto">

       <div class="card" style="height: 700px;">
               <div class="card-header mdi mdi-account-multiple"> Friends</div>
           <div class="card-body">
               <ul class="list-group">
                  <li class="list-group-item">
                    <div class="row">
                      <h7 class="mt-0 mb-1">Friend 1 <small class="text-success">(Online)</small></h7>
                      <img src="http://lorempixel.com/25/25/" alt="<Profile Pic>" class="border border-success rounded-circle ml-auto">
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="row">
                      <h7 class="mt-0 mb-1">Friend 2 <small class="text-warning">(Away)</small></h7>
                      <img src="http://lorempixel.com/25/25/" alt="<Profile Pic>" class="border border-success rounded-circle ml-auto">
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="row">
                      <h7 class="mt-0 mb-1">Friend 3 <small class="text-muted">(Offline)</small></h7>
                      <img src="http://lorempixel.com/25/25/" alt="<Profile Pic>" class="border border-success rounded-circle ml-auto">
                    </div>
                  </li>
               </ul>
           </div>
                      <div class="card-footer">
               <div class="input-group">
                  <input type="text" class="form-control" placeholder="Write a message..." aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <button class="btn input-group-addon mdi mdi-send"> Send</button>
               </div>
           </div>
       </div>
      </div>
