<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');
$current_user = getUserInfo($_SESSION["username"]);
?>
<script>
$(document).ready(function (){
    if(useraway == 0){
        $("#profile-away-state").text("Online");
    } else {
        $("#profile-away-state").text("Away");
    }
});
</script>
<div id="pw_success_alert" class="alert alert-success" role="alert" style="display: none;">Password updated successfully</div>
<div class="card">
<div class="card-header bg-success text-white">Profile</div>
<div class="card-body">


            <div class="row">
                <div class="col-lg-3">

                    <?php
                    if(empty($current_user["avatar"])){
                        echo "<img id='profpic' src='img/default-pic.png' alt='<Profile Pic>' class='mr-3 img-fluid border border-primary rounded-circle' id='profile-profile-pic' style='margin-bottom: 5px; min-width:100%;'>";
                        echo "<input type='hidden' id='lastpic' value='" . "img/default-pic.png" ."'>";
                    } else {
                       echo "<img id='profpic' src='img/profilepics/" . $current_user["avatar"] . "' alt='<Profile Pic>' class='mr-3 img-fluid border border-primary rounded-circle' id='profile-profile-pic' style='margin-bottom: 5px; min-width:100%;'>";
                        echo "<input type='hidden' id='lastpic' value='" . "img/profilepics/" . $current_user["avatar"] . "'>";
                    }
                    ?>
                   <div class="row">
                      <div class="col-sm-12">
                      <form id="profilepicform" action="index.php" method="post" enctype="multipart/form-data">
                      <div class="form-group row"><div class="col-sm-12">

                       <label for="profilepicupload" >
                           Bild hochladen
                           <input id="profilepicupload" type="file" class="form-control-file" id="profilepicupload" name="profilepicupload" enctype="multipart/form-data" onchange="showNewProfilePic(this);" required/>
                       </label>
                      </div></div>
                      <div class="row"><div class="col-sm-6">

                       <input type="submit" style="width: 100%;" class="btn btn-info btn-sm" name="myfileupload" value="OK">
                      </div>
                          <div class="col-sm-6">
                              <button type="button" style="width: 100%;" class="btn btn-light btn-sm" id="resetpic" onclick="resetmypic();" value="OK"> Abbrechen</button>
                          </div></div>
                       </form>
                       </div>
                   </div>

                </div>
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover table-responsive-md">
                                <tr>
                                    <th>Username: </th>
                                    <td id="usr_name"><?= $_SESSION["username"];?></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Email: </th>
                                    <td id="mailtd"><?= $current_user["email"];?></td>
                                    <td><button id="changemail" class="btn btn-info btn-sm mdi mdi-pencil"></button></td>
                                </tr>
                                <tr>
                                    <th>Location: </th>
                                    <td id="profile-loc"></td>
                                    <td><button type="button" id="profile-loc-search" class="btn btn-info btn-sm mdi mdi-cached"></button></td>
                                </tr>
                                <tr>
                                    <th>Status: </th>
                                    <td id="profile-away-state"></td>
                                    <td><button id="profile-away" class="btn btn-info btn-sm">AFK</button></td>
                                </tr>
                                <tr>
                                    <th>Visibility: </th>
                                    <td>Private</td>
                                    <td></td>
                                </tr>
                            </table>
                            <button class="btn btn-sm btn-warning float-right mdi mdi-lock-reset text-white" data-toggle="modal" data-target="#mod-passwd-reset"> Change password</button>
                        </div>
                    </div>
                </div>

    <br>

</div>
</div>

<!-- Modal change password-->
<div class="modal fade" id="mod-passwd-reset" tabindex="-1" role="dialog" aria-labelledby="mod-passwd-reset" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Change password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
           <div id="pw_danger_alert" class="alert alert-danger" role="alert" style="display: none;"></div>
            <div class="form-group">
                <label for="oldpw">Old Password: </label>
                <input type="password" class="form-control" id="oldpw" placeholder="Old Password:">
            </div>
            <div class="form-group">
                <label for="newpw">New Password: </label>
                <input type="password" class="form-control" id="newpw" placeholder="New Password:">
            </div>
            <div class="form-check">
                <label for="rpnewpw">Repeat new Password: </label>
                <input type="password" class="form-control" id="rpnewpw" placeholder="Repeat new Password:">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="chngpw" class="btn btn-primary">Save changes</button>
        </div>
    </div>
  </div>
</div>
