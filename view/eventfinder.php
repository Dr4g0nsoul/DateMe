<script>
$(document).ready(function (){
    if($("#evt-order-all").is(":checked")){
        $.ajax({
            url : 'ajax/ajax_event.php',
            type : 'GET',
            data : {
                eventlisting : "eventlisting",
                listingmethod : "all"
            },
            dataType : 'html',
            async : true,
            success : function(result) {
                $("#evt-results-wrapper").empty();
                $("#evt-results-wrapper").append(result);
            }
        });
    }
});
</script>
<div class="card">
    <div class="card-header bg-info text-white">Find events</div>
    <div class="card-body">
       <div class="row"><div class="col-sm-12">

        <div class="btn-group btn-group-toggle" data-toggle="buttons" aria-pressed="false" role="group">
            <label class="btn btn-secondary active">
                <input type="radio" name="evt-order" id="evt-order-all" autocomplete="off" checked> All
            </label>
            <label class="btn btn-secondary mdi mdi-star-circle">
                <input type="radio" name="evt-order" id="evt-order-popular" autocomplete="off"> Popular
            </label>
            <label class="btn btn-secondary mdi mdi-crosshairs-gps">
                <input type="radio" name="evt-order" id="evt-order-nearby" autocomplete="off"> Nearby
            </label>
            <label class="btn btn-secondary mdi mdi-clock">
                <input type="radio" name="evt-order" id="evt-order-newest" autocomplete="off"> Coming up
            </label>
        </div>
        <span class="float-right"><div class="input-group input-group-sm mb-3" style="margin-bottom: 0;">
  <input id="evt-search-text" type="text" class="form-control" placeholder="Find event" aria-label="Find nemo" aria-describedby="basic-addon2">
  <div class="input-group-append">
    <button id="evt-search" class="btn btn-sm btn-outline-dark mdi mdi-magnify" type="button"> Search</button>
  </div></div>
  <small class="form-text text-muted">Press ESC to remove search filter</small>
</span>
       </div></div>
        <hr>
        <div id="evt-results-wrapper"></div>
    </div>
</div>
