 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tse">
  Launch demo modal
</button>
 <div class="modal fade" id="tse" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Test Event</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <!--<div class="col-lg-3">
                <img class="mr-3 img-fluid" src="http://www.indiantelevision.com/sites/drupal7.indiantelevision.co.in/files/styles/smartcrop_800x800/public/images/tv-images/2017/01/25/Ajit%20Pai.jpg?itok=5EK7rKsq" width="200px" height="200px" alt="Generic placeholder image">
            </div>-->
            <div class="col-lg-12">
                <div class="row">

                    <h6 class="mt-0 ml-3" style="padding-top: 10px;"><strong>Not fake news</strong></h6><p class="ml-3">
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 class="text-right">Join Event: </h6>
            </div>
            <div class="col-lg-9">
                <button class="btn btn-info btn-sm mdi mdi-plus"> Add to calendar</button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <h6 class="text-right">Rating: </h6>
            </div>
            <div class="col-lg-9">
                <p><span class="mdi mdi-star"></span> <span class="mdi mdi-star"></span> <span class="mdi mdi-star"></span> <span class="mdi mdi-star"></span> <span class="mdi mdi-star-outline"></span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 class="text-right">Start date: </h6>
            </div>
            <div class="col-lg-9">
                <p><span class="mdi mdi-calendar"></span> 10.01.2017 &emsp;<span class="mdi mdi-clock"></span> 10:30</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 class="text-right">End date: </h6>
            </div>
            <div class="col-lg-9">
                <p><span class="mdi mdi-calendar"></span> 10.01.2017 &emsp;<span class="mdi mdi-clock"></span> 10:30</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <h6 class="text-right">Location: </h6>
            </div>
            <div class="col-lg-9">
                <p>Somestreet 18, New Donk City, Instantbully</p>
            </div>
        </div>
      </div>
      <div class="row">
            <div class="col-lg-3">

            </div>
            <div class="col-lg-9">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim sit voluptatibus velit, illo fuga cumque accusamus maxime non molestiae minus sed, corporis distinctio sapiente eius nihil consectetur iure praesentium, veritatis voluptatem. Optio magnam, quibusdam aut recusandae officia, at similique commodi accusamus, maxime distinctio corrupti, quae ratione iure voluptatum minus praesentium. Neque, molestias, alias. Nihil cum, sint optio repellat doloribus fugit dignissimos dolor, odit corporis tempora provident quo voluptatum voluptatibus sapiente! Esse obcaecati earum repudiandae, rem cumque totam sed soluta! Et in odio iusto delectus laudantium expedita natus quidem laborum. Nemo nostrum veritatis, vel laborum ipsum. Est aspernatur velit cumque at.</p>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
