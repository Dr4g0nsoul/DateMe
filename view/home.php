<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/events.php');
$feat_events = getPopularEvents(true);
?>
       <div class="card">
               <div class="card-header bg-info text-white">Featured events</div>
           <div class="card-body">
<?php
foreach($feat_events as $curr){
?>
                <div class="card">
                    <div class="card-header"><span class="float-left text-info text-right mdi mdi-calendar" style="
                        padding-top: 4px;"> <?= date("D d.m.Y",strtotime($curr["start_date"]))?>  <span class="mdi mdi-clock"> <?= date("H:i",strtotime($curr["start_date"]))?></span></span><span class="float-right"><button eventid="<?= $curr["id"]?>" class="btn btn-sm btn-info showevent">More info...</button></span></div>
                   <div class="card-body">
                       <h5 class="card-title"><?= $curr["title"]?></h5>
                       <?php if(!empty($curr["thumbnail"])){?>
                       <div class="row">
                       <div class="col-lg-3">

                          <img class="mr-3 img-fluid" src="img/event_thumb/<?= $curr["thumbnail"]?>" width="200px" height="200px" alt="Generic placeholder image">
                       </div>
                       <div class="col-lg-9">
                           <div class="row">

                           <p class="mr-3">
                           <?= $curr["description"]?></p>
                           </div>
                       </div>
                    </div>
                    <?php } else {?>
                    <div class="row">
                       <div class="col-lg-12">
                           <div class="row">

                           <p class="mr-3">
                           <?= $curr["description"]?></p>
                           </div>
                       </div>
                    </div>
                    <?php }?>
                   </div>

                </div>
                <br>
<?php }?>
           </div>
       </div>


