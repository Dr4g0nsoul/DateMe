<?php require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php'); ?>
<script>
$(document).ready(function (){
    currentSchedDay = 1;
    $('#sched-table').empty();
    loadScheduleIntoTable();
});
</script>
<input type="hidden" id="schedusrid" value="<?= getUserInfo($_SESSION["username"])["id"];?>">
<div class="card">
        <div class="card-header mdi mdi-clock"> Schedule <span class="float-right" style="height: 30px;"><div class="input-group input-group-sm mb-3">
  <div class="input-group-prepend">
    <button id="sched-previous" class="btn btn-sm btn-info" type="button">&lt;-</button>
  </div>
  <input type="text" class="form-control text-center" id="sched-month" placeholder="" aria-label="" aria-describedby="basic-addon1" readonly value="Month">
<div class="input-group-append">
    <button id="sched-next" class="btn btn-sm btn-info" type="button">-&gt;</button>
  </div></div></span>
        </div>
        <div class="card-body">
            <table class="table table-hover table-responsive-md">
               <tbody id="sched-table">
                <tr class="table-primary">
                    <td class="mdi mdi-calendar"> Monday 07.01.2018</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="mdi mdi-clock"> 13:20</td>
                    <td>Test event</td>
                    <td class="float-right"><button class="btn btn-sm btn-light mdi mdi-magnify"></button> <button class="btn btn-sm btn-light mdi mdi-close-circle-outline"></button></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
