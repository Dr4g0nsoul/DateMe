<?php extract($GLOBALS); ?>
<!-- Top content -->
      <meta name="viewport" content="width=device-width" />
       <link rel="stylesheet" href="../css/loginstyle.css?v=19">
       <script>

$(document).ready(function(){
    if (navigator.geolocation){
        $(document).on("click","#loginSearchLocation",function(){
            navigator.geolocation.getCurrentPosition(function(position){
                $("#user-location").text(position.coords.latitude+", "+ position.coords.longitude);
                alert("Lat: "+position.coords.latitude+" Long: "+ position.coords.longitude);
                $.ajax({
                    url : 'https://maps.googleapis.com/maps/api/geocode/json',
                    type : 'GET',
                    data : {
                        latlng : $("#user-location").text(),
                        key : "<?= $google_key;?>"
                    },
                    dataType : 'json',
                    async : true,
                    success : function(result) {
                        console.log(JSON.stringify(result["results"][0]["formatted_address"]));
                        $("#user-location").text(JSON.stringify(result["results"][0]["formatted_address"]));
                    }
                });
            });
        });

    } else {
        $('#user-location').val("GPS not detected");
        $('#loginSearchLocation').prop("disabled","disabled");
    }
});

function registration(){
    if($.trim($("#form-reg-password").val()) === $.trim($("#form-reg-password-2").val())) {
        var pw = $.trim($("#form-reg-password").val());
        if(pw.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)){
            $("#reg_btn").prop("disabled","disabled");
            $.ajax({
                url : 'ajax/ajax_users.php',
                    type : 'POST',
                    data : {
                        register : "register",
                        username : $.trim($("#form-reg-username").val()),
                        password : $.trim($("#form-reg-password").val()),
                        email : $.trim($("#form-email").val())
                    },
                    dataType : 'text',
                    async : true,
                    success : function(result) {
                        if(result.toLowerCase() === "false") {
                            $("#login_danger_alert").text("Unable to conclude the registration.");
                            if(!$("#login_danger_alert").is(":visible")) {
                                $("#login_danger_alert").slideDown("slow");
                            }
                        } else {
                            setTimeout(function(){
                               location.reload();
                            }, 1000);
                        }
                    }
            });
            setTimeout(function() {$("#reg_btn").removeProp("disabled");}, 3000);
            return false;
        } else {
            $("#login_danger_alert").text("The password has to atleast contain one Uppercase, one lowercase character, one number and it has to  be atleas 8 characters long.");
            if(!$("#login_danger_alert").is(":visible")) {
                $("#login_danger_alert").slideDown("slow");
            }
            return false;
        }
    } else {
        $("#login_danger_alert").text("The two passwords are not coinciding.");
        if(!$("#login_danger_alert").is(":visible")) {
            $("#login_danger_alert").slideDown("slow");
        }
        return false;
    }
}

function log_in(){
    $("#login_btn").prop("disabled","disabled");
    $.ajax({
                url : 'ajax/ajax_users.php',
                    type : 'POST',
                    data : {
                        login : "login",
                        username : $.trim($("#form-username").val()),
                        password : $.trim($("#form-password").val())
                    },
                    dataType : 'text',
                    async : true,
                    success : function(result) {
                        if(result.toLowerCase() === "false") {
                            $("#login_danger_alert").text("Invalid username or password.");
                            if(!$("#login_danger_alert").is(":visible")) {
                                $("#login_danger_alert").slideDown("slow");
                            }
                        } else {
                            setTimeout(function(){
                               location.reload();
                            }, 1000);
                        }
                    }
            });
    setTimeout(function() {$("#login_btn").text("Logging in.");}, 500);
    setTimeout(function() {$("#login_btn").text("Logging in..");}, 1000);
    setTimeout(function() {$("#login_btn").text("Logging in...");}, 2000);
    setTimeout(function() {$("#login_btn").text("Sign In!");$("#login_btn").removeAttr("disabled"); console.log("x");}, 3000);
    return false;
}


        </script>
        <div class="top-content">

            <div class="inner-bg">

                <div class="container">

                    <div class="row" style="padding-top: 20px;">
                        <div class="col-sm-8 col-sm-offset-2 text mx-auto" >
                            <h1>Welcome to <strong>WeVent</strong></h1>
                            <div class="description">
                            	<p>
	                            	An event promotion and organization platform
                            	</p>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                         <div class="col-sm-12">
                            <div id="login_danger_alert" class="alert alert-danger" role="alert" style="display: none;"></div>
                         </div>

                      </div>

                    <div class="row">
                       <div class="col-sm-5">

                        	<div class="form-box">
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Sign up now</h3>
	                            		<p>Fill in the form below to get instant access:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="mdi mdi-lead-pencil"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" class="registration-form" onsubmit="return registration();">
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-reg-username">Username</label>
				                        	<input type="text" name="form-reg-username" placeholder="Username..." class="form-control" id="form-reg-username" required>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-reg-password">Password</label>
				                        	<input type="password" name="form-reg-password" placeholder="Password..." class="form-control" id="form-reg-password" required>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-reg-password-2">Retype Password</label>
				                        	<input type="password" name="form-reg-password-2" placeholder="Retype Password..." class="form-control" id="form-reg-password-2" required>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-email">Email</label>
				                        	<input type="email" name="form-email" placeholder="Email..." class="form-control" id="form-email" required>
				                        </div>
				                        <!--
				                        <div class="form-group">
                                                <label class="sr-only" for="form-about-yourself">Get Position</label>
                                                <textarea id="user-location" type="text" style="width: 100%;"></textarea>

				                        	    <button type="button" class="btn btn-warning mdi mdi-search-web" id="loginSearchLocation" style="font-size: 30px; padding: 0px 5px 5px 5px; margin-top: 5px;"></button>
				                        </div>
				                        -->
				                        <button id="reg_btn" type="submit" class="btn btn-success" name="register">Sign me up!</button>
				                    </form>
			                    </div>
                        	</div>

                        </div>


                        <div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>

                        <div class="col-sm-5">

                        	<div class="form-box login-form-thing">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Login: </h3>
	                            		<p>Enter username and password to log on:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="mdi mdi-lock-outline"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form class="login-form" onsubmit="return log_in();">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" required>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password" required>
				                        </div>
				                        <button id="login_btn" type="submit" class="btn btn-info" name="login">Sign in!</button>
				                    </form>
			                    </div>
		                    </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
