<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/config.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/database.php');

    function user_login($username, $password) {
        extract($GLOBALS);
        if(!empty($username)&&!empty($password)) {
            $password = hash('sha256', $salt.$password, false);
            $mysqli = connectDB();
            $result = $mysqli->query("SELECT * FROM users WHERE username='" . $username . "' AND password = '" . $password . "'");
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                closeDB($mysqli);
                return $row["username"];
            }
        }
        return false;
    }

    function user_register($username, $password, $email, $location) {
        extract($GLOBALS);
        if(!empty($username)&&!empty($password)&&!empty($email)) {
            $password = hash('sha256', $salt.$password, false);
            $k = array(
                "username",
                "password",
                "email"
            );
            $v = array(
                $username,
                $password,
                $email
            );
            if(table_insert("users",$k,$v)){
                return $username;
            }
        }
        return false;
    }

    function user_change_password($username, $oldpw, $newpw) {
        extract($GLOBALS);
        if(!empty($username)&&!empty($oldpw)&&!empty($newpw)) {
            //check if user exists
            $oldpw = hash('sha256', $salt.$oldpw, false);
            $mysqli = connectDB();
            $result = $mysqli->query("SELECT * FROM users WHERE username='" . $username . "' AND password = '" . $oldpw . "'");
            if ($result->num_rows > 0) {
                closeDB($mysqli);
                //change password
                $newpw = hash('sha256', $salt.$newpw, false);
                if(table_update("users", array("password"), array($newpw), "username = '" . $username . "'")){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /*
    * Result:
    * array (
    *   "id" -> 1,
    *   "username" -> bestmanat69,
    *   "email" -> homer@simsons.net,
    *   "avatar" -> "path/to/img/location.png",
    *   "location" -> array(
    *       "id" -> 1,
    *       "desc" -> "locationdesc.",
    *       "long" -> 1.123123,
    *       "lat" -> 5.234324
    *    ),
    *   "pub_stars" -> 5,
    *   "priv_stars" -> 20,
    *   "status" -> 0,
    *   "friends" -> array("friendusername1","friend2username")
    *);
    * or false
    *
    * Status:
    * 0 -> Offline,
    * 1 -> Online,
    * 2 -> AFK
    */
    function getUserInfo($username){
        extract($GLOBALS);
        if(!empty($username)){
            $ret = array();
            $mysqli = connectDB();
            $sql = "SELECT * FROM users WHERE username = '" . $username . "'";
            if($result = $mysqli -> query($sql)){
                if($result -> num_rows > 0){
                    $row = $result -> fetch_assoc();
                    if(isset($row["id"])){$ret["id"] = $row["id"];}
                    if(isset($row["username"])){$ret["username"] = $row["username"];}
                    if(isset($row["email"])){$ret["email"] = $row["email"];}
                    if(isset($row["avatar"])){$ret["avatar"] = $row["avatar"];}
                    /*
                    if(isset($row["location_id"])){
                        $loc = array();
                        $loc["id"] = intval($row["location_id"]);
                        if(isset($row["description"])){$loc["desc"] = $row["description"];}
                        if(isset($row["longitude"])){$loc["long"] = doubleval($row["longitude"]);}
                        if(isset($row["latitude"])){$loc["lat"] = doubleval($row["latitude"]);}
                        $ret["location"] = $loc;
                    }
                    */
                    if(isset($row["public_stars"])){
                        $ret["public_stars"] = intval($row["public_stars"]);
                    } else {
                       $ret["public_stars"] = 0;
                    }
                    if(isset($row["private_stars"])){
                        $ret["private_stars"] = intval($row["private_stars"]);
                    } else {
                        $ret["private_stars"] = 0;
                    }
                    if(isset($row["status"])){$ret["status"] = intval($row["status"]);}
                    closeDB($mysqli);
                    return $ret;
                } else {
                    echo "getUserInfo: user " . $username . " not found!";
                }
            } else {
                echo "query failed in getUserInfo: " . $sql;
                return false;
            }
        } else {
            return false;
        }
    }

    function getUserId($username) {
        extract($GLOBALS);
        if(!empty($username)){

            $mysqli = connectDB();
            if($result = $mysqli -> query("SELECT id FROM users WHERE username = '" . $username . "'")){
                if($result -> num_rows > 0){
                    $row = $result -> fetch_assoc();
                    $ret = $row["id"];
                    closeDB($mysqli);
                    return $ret;
                }
            }
        }
        return false;
    }

?>
