<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/config.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/users.php');

    function createEvent($username, $title, $description, $thumbnail, $start_date, $end_date, $location, $is_public = false) {
        if(!empty($username)&&!empty($title)&&!empty($description)&&!empty($start_date)&&strtotime($start_date)){
            if($user_id = getUserId($username)){
                $k = array("user_host", "title", "description", "start_date");
                $v = array($user_id, $title, $description, date("Y-m-d H:i:s",strtotime($start_date)));
                if(!empty($end_date)&&strtotime($end_date)){
                    array_push($k, "end_date");
                    array_push($v, date("Y-m-d H:i:s",strtotime($end_date)));
                }
                if(!empty($thumbnail)){
                    array_push($k, "thumbnail");
                    array_push($v, $thumbnail);
                }
                array_push($k, "public");
                if($is_public){
                    array_push($v, 1);
                } else {
                    array_push($v, 0);
                }

                if(!empty($location)&&!empty($location["desc"])&&isset($location["lat"])&&isset($location["long"])){
                    if($loc_id = table_insert_with_id("locations", array("description", "latitude", "longitude"), array($location["desc"], floatval($location["lat"]), floatval($location["long"])))){
                        array_push($k, "location_id");
                        array_push($v, intval($loc_id));
                    }
                }

                //insert into database
                if(table_insert("events", $k, $v)){
                    return true;
                }
            }
        }
        return false;
    }

    function deleteEvent($id){
        if(table_update("events", array("deleted"), array(1), "id = ".$id)){
            if(table_delete("eventparticipants", "event_id = " . $id)){
                return true;
            }
        }
        return false;
    }

    /*
    * Result:
    * array (
    *   "id" -> 1,
    *   "host" -> bestmanat69,
    *   "title" -> "TITLE",
    *   "description" -> "desc",
    *   "thumbnail" -> "path/to/img/location.png",
    *   "participant_count" -> 12,
    *   "start_date" -> datetime,
    *   "end_date" -> datetime,
    *   "location" -> array(
    *       "id" -> 1,
    *       "desc" -> "locationdesc.",
    *       "long" -> 1.123123,
    *       "lat" -> 5.234324
    *    ),
    *   "public" -> true or false,
    *   "status" -> 0,
    *);
    * or false
    */
    function getEvent($id){
        $ret = array();
        if(!empty($id)){
            $mysqli = connectDB();
            $sql = "SELECT e.*, u.username, l.description As 'desc', l.longitude As 'long', l.latitude As 'lat' FROM events e LEFT JOIN users u ON e.user_host = u.id LEFT JOIN locations l ON e.location_id = l.id WHERE e.id = " . $id . " AND e.deleted = 0";
            if($result = $mysqli -> query($sql)){
                if($result -> num_rows > 0) {
                    $row = $result -> fetch_assoc();
                    $ret["id"] = $row["id"];
                    if(!empty($row["username"])){$ret["username"] = $row["username"];}
                    if(!empty($row["user_host"])){$ret["host"] = intval($row["user_host"]);}
                    if(!empty($row["title"])){$ret["title"] = $row["title"];}
                    if(!empty($row["description"])){$ret["description"] = $row["description"];}
                    if(!empty($row["thumbnail"])){$ret["thumbnail"] = $row["thumbnail"];}
                    if(!empty($row["participant_count"])){$ret["participant_count"] = intval($row["participant_count"]);} else {$ret["participant_count"] = 0;}
                    if(!empty($row["start_date"]) && strtotime($row["start_date"])){$ret["start_date"] = $row["start_date"];}
                    if(!empty($row["end_date"]) && strtotime($row["end_date"])){$ret["end_date"] = $row["end_date"];}
                    if(!empty($row["desc"]) && !empty($row["lat"]) && !empty($row["long"])){
                        $ret["location"] = array(
                            "desc" => $row["desc"],
                            "lat" => floatval($row["lat"]),
                            "long" => floatval($row["long"])
                        );
                    }
                    if(!empty($row["public"])){$ret["public"] = true;} else {$ret["public"] = true;}
                    return $ret;
                }
            }
        }
        return false;
    }

    function joinEvent($user_id, $event_id){
        if (table_insert("eventparticipants", array("user_id", "event_id"), array($user_id, $event_id))){
            $mysqli = connectDB();
            if($mysqli -> query("UPDATE events SET participant_count = participant_count + 1 WHERE id = " . $event_id)){
                closeDB($mysqli);
                return true;
            }
            closeDB($mysqli);
        }
        return false;
    }

    function leaveEvent($user_id, $event_id){
        if (table_delete("eventparticipants", "user_id = " . $user_id . " AND event_id = " . $event_id)){
            $mysqli = connectDB();
            if($mysqli -> query("UPDATE events SET participant_count = participant_count - 1 WHERE id = " . $event_id)){
                closeDB($mysqli);
                return true;
            }
            closeDB($mysqli);
        }
        return false;
    }

    function isParticipating($user_id, $event_id) {
        $mysqli = connectDB();
        if($result = $mysqli->query("SELECT * FROM eventparticipants WHERE user_id = " . $user_id . " AND event_id = " . $event_id)){
            if($result -> num_rows > 0){
                closeDB($mysqli);
                return true;
            }
        }
        closeDB($mysqli);
        return false;
    }

    function getPublicEvents($search = ""){
        $ret = array();
        $mysqli = connectDB();
        $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' ORDER BY title";
        if(!empty($search)){
            $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' AND title LIKE '%" . $search . "%' ORDER BY title";
        }
        if($result = $mysqli -> query($sql)){
            while($row = $result -> fetch_assoc()){
                $evt = array();
                $evt["id"] = $row["id"];
                if(!empty($row["username"])){$evt["username"] = $row["username"];}
                if(!empty($row["user_host"])){$evt["host"] = intval($row["user_host"]);}
                if(!empty($row["title"])){$evt["title"] = $row["title"];}
                if(!empty($row["description"])){$evt["description"] = $row["description"];}
                if(!empty($row["thumbnail"])){$evt["thumbnail"] = $row["thumbnail"];}
                if(!empty($row["participant_count"])){$evt["participant_count"] = intval($row["participant_count"]);} else {$evt["participant_count"] = 0;}
                if(!empty($row["start_date"]) && strtotime($row["start_date"])){$evt["start_date"] = $row["start_date"];}
                if(!empty($row["end_date"]) && strtotime($row["end_date"])){$evt["end_date"] = $row["end_date"];}
                if(!empty($row["public"])){$evt["public"] = true;} else {$evt["public"] = false;}
                array_push($ret, $evt);
            }
        }
        closeDB($mysqli);
        return $ret;
    }

    function getPopularEvents($featured = false, $search = ""){
        $ret = array();
        $mysqli = connectDB();
        $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' ORDER BY participant_count DESC";
        if(!empty($search)){
            $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' AND title LIKE '%" . $search . "%' ORDER BY participant_count DESC";
        }
        if($featured){
            $sql .= " LIMIT 2";
        }
        if($result = $mysqli -> query($sql)){
            while($row = $result -> fetch_assoc()){
                $evt = array();
                $evt["id"] = $row["id"];
                if(!empty($row["username"])){$evt["username"] = $row["username"];}
                if(!empty($row["user_host"])){$evt["host"] = intval($row["user_host"]);}
                if(!empty($row["title"])){$evt["title"] = $row["title"];}
                if(!empty($row["description"])){$evt["description"] = $row["description"];}
                if(!empty($row["thumbnail"])){$evt["thumbnail"] = $row["thumbnail"];}
                if(!empty($row["participant_count"])){$evt["participant_count"] = intval($row["participant_count"]);} else {$evt["participant_count"] = 0;}
                if(!empty($row["start_date"]) && strtotime($row["start_date"])){$evt["start_date"] = $row["start_date"];}
                if(!empty($row["end_date"]) && strtotime($row["end_date"])){$evt["end_date"] = $row["end_date"];}
                if(!empty($row["public"])){$evt["public"] = true;} else {$evt["public"] = false;}
                array_push($ret, $evt);
            }
        }
        closeDB($mysqli);
        return $ret;
    }

    function getNewestEvents($search = ""){
        $ret = array();
        $mysqli = connectDB();
        $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' ORDER BY start_date";
        if(!empty($search)){
            $sql = "SELECT * FROM events WHERE public = 1 AND deleted = 0 AND start_date > '" . date("Y-m-d H:i:s") . "' AND title LIKE '%" . $search . "%' ORDER BY start_date";
        }
        if($result = $mysqli -> query($sql)){
            while($row = $result -> fetch_assoc()){
                $evt = array();
                $evt["id"] = $row["id"];
                if(!empty($row["username"])){$evt["username"] = $row["username"];}
                if(!empty($row["user_host"])){$evt["host"] = intval($row["user_host"]);}
                if(!empty($row["title"])){$evt["title"] = $row["title"];}
                if(!empty($row["description"])){$evt["description"] = $row["description"];}
                if(!empty($row["thumbnail"])){$evt["thumbnail"] = $row["thumbnail"];}
                if(!empty($row["participant_count"])){$evt["participant_count"] = intval($row["participant_count"]);} else {$evt["participant_count"] = 0;}
                if(!empty($row["start_date"]) && strtotime($row["start_date"])){$evt["start_date"] = $row["start_date"];}
                if(!empty($row["end_date"]) && strtotime($row["end_date"])){$evt["end_date"] = $row["end_date"];}
                if(!empty($row["public"])){$evt["public"] = true;} else {$evt["public"] = false;}
                array_push($ret, $evt);
            }
        }
        closeDB($mysqli);
        return $ret;
    }

    function getNearestEvents($lat, $long, $search = ""){
        $ret = array();
        if(!empty($lat) && !empty($long)){
            $mysqli = connectDB();
            $sql = "SELECT e.* FROM events e JOIN locations l ON e.location_id = l.id AND e.public = 1 AND e.deleted = 0 AND e.start_date > '" . date("Y-m-d H:i:s") . "' ORDER BY ACOS(SIN(" . floatval($lat) . "*0.0174532778)*SIN(l.latitude*0.0174532778) + COS(" . floatval($lat) . "*0.0174532778)*COS(l.latitude*0.0174532778)*COS((" . floatval($long) . "-l.longitude)*0.0174532778))";
            if(!empty($search)){
                $sql = "SELECT e.* FROM events e JOIN locations l ON e.location_id = l.id AND e.title LIKE '%%' AND e.public = 1 AND e.deleted = 0 AND e.start_date > '" . date("Y-m-d H:i:s") . "' ORDER BY ACOS(SIN(" . floatval($lat) . "*0.0174532778)*SIN(l.latitude*0.0174532778) + COS(" . floatval($lat) . "*0.0174532778)*COS(l.latitude*0.0174532778)*COS((" . floatval($long) . "-l.longitude)*0.0174532778))";
            }
            if($result = $mysqli -> query($sql)){
                while($row = $result -> fetch_assoc()){
                    $evt = array();
                    $evt["id"] = $row["id"];
                    if(!empty($row["username"])){$evt["username"] = $row["username"];}
                    if(!empty($row["user_host"])){$evt["host"] = intval($row["user_host"]);}
                    if(!empty($row["title"])){$evt["title"] = $row["title"];}
                    if(!empty($row["description"])){$evt["description"] = $row["description"];}
                    if(!empty($row["thumbnail"])){$evt["thumbnail"] = $row["thumbnail"];}
                    if(!empty($row["participant_count"])){$evt["participant_count"] = intval($row["participant_count"]);} else {$evt["participant_count"] = 0;}
                    if(!empty($row["start_date"]) && strtotime($row["start_date"])){$evt["start_date"] = $row["start_date"];}
                    if(!empty($row["end_date"]) && strtotime($row["end_date"])){$evt["end_date"] = $row["end_date"];}
                    if(!empty($row["public"])){$evt["public"] = true;} else {$evt["public"] = false;}
                    array_push($ret, $evt);
                }
            }
            closeDB($mysqli);
        }
        return $ret;
    }

    /*
    * Result:
    * array (
    *   array(
    *       "id" -> 1,
    *       "day" -> 17,
    *       "public" -> true
    *   ), ...
    *);
    */
    function calendarfetch($user_id, $month, $year){
        $ret = array();
        $mysqli = connectDB();
        $sql = "SELECT e.id, DAY(e.start_date) AS 'day', e.public FROM events e LEFT JOIN eventparticipants ep ON ep.event_id = e.id WHERE MONTH(e.start_date) = " . $month . " AND YEAR(e.start_date) = " . $year . " AND (e.user_host = " . $user_id . " OR ep.user_id = " . $user_id . ") AND e.deleted = 0 ORDER BY e.start_date";
        if($result = $mysqli -> query($sql)){
            while($row = $result -> fetch_assoc()){
                $entry = array();
                $entry["id"] = $row["id"];
                $entry["day"] = $row["day"];
                if(!empty($row["public"])){
                    $entry["public"] = true;
                } else {
                    $entry["public"] = false;
                }
                array_push($ret, $entry);
            }
        }
        closeDB($mysqli);
        return $ret;
    }

    /*
    * Result:
    * array (
    *   array(
    *       "id" -> 1,
    *       "title" -> "Eventtitle",
    *       "start_date" -> "2018-12-01 20:30",
    *       "public" -> true
    *   ), ...
    *);
    */
    function schedulefetch($user_id, $month, $year){
        $ret = array();
        $mysqli = connectDB();
        $sql = "SELECT e.id, e.user_host, e.title, e.start_date, e.public FROM events e LEFT JOIN eventparticipants ep ON ep.event_id = e.id WHERE MONTH(e.start_date) = " . $month . " AND YEAR(e.start_date) = " . $year . " AND (e.user_host = " . $user_id . " OR ep.user_id = " . $user_id . ") AND e.deleted = 0 ORDER BY e.start_date";
        if($result = $mysqli -> query($sql)){
            while($row = $result -> fetch_assoc()){
                $entry = array();
                $entry["id"] = $row["id"];
                $entry["host"] = $row["user_host"];
                $entry["title"] = $row["title"];
                $entry["start_date"] = $row["start_date"];
                if(!empty($row["public"])){
                    $entry["public"] = true;
                } else {
                    $entry["public"] = false;
                }
                array_push($ret, $entry);
            }
        }
        closeDB($mysqli);
        return $ret;
    }

?>
