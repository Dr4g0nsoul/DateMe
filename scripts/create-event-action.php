<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/events.php');
if(!empty($_POST)&&!empty($_POST["ec-username"])&&!empty($_POST["ec-title"])&&!empty($_POST["ec-description"])&&!empty($_POST["ec-start-date"])&&!empty($_POST["ec-start-time"])&&!empty($_POST["ec-visibility"])){
    $is_public = false;
    if($_POST["ec-visibility"] == "public"){
        $is_public = true;
    }
    $end_date = null;
    if(!empty($_POST["ec-end-time"])&&!empty($_POST["ec-end-date"])){
        $end_date = $_POST["ec-end-date"] . ' ' . $_POST["ec-end-time"];
    }
    //upload thumbnail
    $thumbnail = "";
    if(!empty($_FILES['ec-thumbnail']["name"])) {

        // Create directory if it does not exist
        if(!is_dir("../img/event_thumb/")) {
            mkdir("../img/event_thumb/", 0777, true);
        }
        $thumbnail = basename(strtolower(strtr($_FILES['ec-thumbnail']["name"], array(' ' => '_','ä' => 'ae','ö' => 'oe','ü' => 'ue','ß' => 'ss'))));
        $nameaddition = basename(strtolower(strtr($_POST["ec-username"] . $_POST["ec-start-date"], array(' ' => '_','ä' => 'ae','ö' => 'oe','ü' => 'ue','ß' => 'ss'))));
        $thumbnail = $thumbnail . $nameaddition . substr($thumbnail,strrpos($thumbnail, '.'));
        move_uploaded_file($_FILES['ec-thumbnail']['tmp_name'], "../img/event_thumb/".$thumbnail);
    }
    //set location
    $location = array();
    if(!empty($_POST["ec-location"]) && isset($_POST["ec-lat"]) && isset($_POST["ec-long"])){
        $location = array(
            "desc" => $_POST["ec-location"],
            "lat" => floatval($_POST["ec-lat"]),
            "long" => floatval($_POST["ec-long"])
        );
    }
    createEvent($_POST["ec-username"], $_POST["ec-title"], $_POST["ec-description"], $thumbnail, $_POST["ec-start-date"] . ' ' . $_POST["ec-start-time"], $end_date, $location, $is_public);
}

header('Location: ../index.php');
?>
