<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/scripts/config.php');

    function connectDB() {
        extract($GLOBALS);
        /*
        * Verbindungsaufbau
        */

        $mysqli = new mysqli($db_host, $db_user, $db_password, $db_database);

        if ($mysqli->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return false;
        }

        //$mysqli->query("SET NAMES 'utf8'");


        if(!$mysqli->query("START TRANSACTION")) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            return false;
        }

        unset($result);

        return $mysqli;
    }

    function closeDB($mysqli) {
        $mysqli->query("COMMIT");
        $mysqli->close();
    }


    function table_update($table, $name_arr, $value_arr, $where) {
        if(is_array($name_arr) && is_array($value_arr) && count($name_arr) === count($value_arr) && !empty($table)) {
            //Create query
            $sql = "UPDATE " . $table . " SET ";
            for($i = 0; $i < count($name_arr); $i += 1) {
                if(is_numeric($value_arr[$i])||is_bool($value_arr[$i])) {
                    $sql .= $name_arr[$i] . " = " . $value_arr[$i] . ", ";
                } elseif(strtotime($value_arr[$i])){
                    $sql .= $name_arr[$i] . " = '" . date("Y-m-d H:i:s", strtotime($value_arr[$i])) . "', ";
                } else {
                    $sql .= $name_arr[$i] . " = '" . $value_arr[$i] . "', ";
                }
            }
            $sql = substr($sql, 0, strrpos($sql,','));
            if(!empty($where)) {
                $sql .= " WHERE " . $where;
                if(!strpos($sql,';')){
                    $sql .= ";";
                }
            }
            //Connect and execute query
            $mysqli = connectDB();
            if(!$mysqli->connect_errno){
                if(!$mysqli->query($sql)) {
                    echo "Failed to execute query: " . $sql;
                    return false;
                } else {
                    closeDB($mysqli);
                    return true;
                }

            }
        } else {
            return false;
        }
    }

    function table_insert($table, $name_arr, $value_arr){
        if(is_array($name_arr) && is_array($value_arr) && count($name_arr) === count($value_arr) && !empty($table)) {
            //Create query
            $sql = "INSERT INTO " . $table . "( ";
            for($i = 0; $i < count($name_arr); $i += 1){
                $sql .= $name_arr[$i] . ", ";
            }
            $sql = substr($sql, 0, strrpos($sql, ','));
            $sql .= ") VALUES (";
            for($i = 0; $i < count($name_arr); $i += 1) {
                if(is_numeric($value_arr[$i])||is_bool($value_arr[$i])) {
                    $sql .= $value_arr[$i] . ", ";
                } elseif(strtotime($value_arr[$i])){
                    $sql .= "'" . date("Y-m-d H:i:s", strtotime($value_arr[$i])) . "', ";
                } else {
                    $sql .= "'" . $value_arr[$i] . "', ";
                }
            }
            $sql = substr($sql, 0, strrpos($sql,','));
            $sql .= ");";
            //Connect and execute query
            $mysqli = connectDB();
            if(!$mysqli->connect_errno){
                if(!$mysqli->query($sql)) {
                    echo "Failed to execute query: " . $sql;
                    return false;
                } else {
                    closeDB($mysqli);
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    function table_insert_with_id($table, $name_arr, $value_arr){
        if(is_array($name_arr) && is_array($value_arr) && count($name_arr) === count($value_arr) && !empty($table)) {
            //Create query
            $sql = "INSERT INTO " . $table . "( ";
            for($i = 0; $i < count($name_arr); $i += 1){
                $sql .= $name_arr[$i] . ", ";
            }
            $sql = substr($sql, 0, strrpos($sql, ','));
            $sql .= ") VALUES (";
            for($i = 0; $i < count($name_arr); $i += 1) {
                if(is_numeric($value_arr[$i])||is_bool($value_arr[$i])) {
                    $sql .= $value_arr[$i] . ", ";
                } elseif(strtotime($value_arr[$i])){
                    $sql .= "'" . date("Y-m-d H:i:s", strtotime($value_arr[$i])) . "', ";
                } else {
                    $sql .= "'" . $value_arr[$i] . "', ";
                }
            }
            $sql = substr($sql, 0, strrpos($sql,','));
            $sql .= ");";
            //Connect and execute query
            $mysqli = connectDB();
            if(!$mysqli->connect_errno){
                if(!$mysqli->query($sql)) {
                    echo "Failed to execute query: " . $sql;
                    return false;
                } else {
                    $insId = $mysqli -> insert_id;
                    closeDB($mysqli);
                    return $insId;
                }
            }
        } else {
            return false;
        }
    }

    function table_delete($table, $where){
        if(!empty($table)) {
            //Create query
            $sql = "DELETE FROM " . $table;
            if(!empty($where)) {
                $sql .= " WHERE " . $where;
            }
            $sql .= ";";
            //Connect and execute query
            $mysqli = connectDB();
            if(!$mysqli->connect_errno){
                if(!$mysqli->query($sql)) {
                    echo "Failed to execute query: " . $sql;
                    return false;
                } else {
                    closeDB($mysqli);
                    return true;
                }
            }
        } else {
            return false;
        }
    }

?>
